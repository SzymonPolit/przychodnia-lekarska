package com.example.przychodnia_lekarska_spring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PrzychodniaLekarskaSpringApplicationTests {

    @Test
    public void contextLoads() {
    }

}
