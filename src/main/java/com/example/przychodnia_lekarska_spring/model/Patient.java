package com.example.przychodnia_lekarska_spring.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Patient {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // patientId
    // patientID



    private String name;
    private String surname;
    private int age;
    private String requiredDoctorProfession;
    private long PESEL;

    public Patient(String name, String surname, int age, String requiredDoctorProfession, long PESEL) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.requiredDoctorProfession = requiredDoctorProfession;
        this.PESEL = PESEL;
    }

    public Patient() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getRequiredDoctorProfession() {
        return requiredDoctorProfession;
    }

    public void setRequiredDoctorProfession(String requiredDoctorProfession) {
        this.requiredDoctorProfession = requiredDoctorProfession;
    }

    public long getPESEL() {
        return PESEL;
    }

    public void setPESEL(long PESEL) {
        this.PESEL = PESEL;
    }


    public String toString(){
        return getName()+" "+getSurname();
    }

}
