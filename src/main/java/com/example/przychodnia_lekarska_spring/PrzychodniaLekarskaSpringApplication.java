package com.example.przychodnia_lekarska_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrzychodniaLekarskaSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrzychodniaLekarskaSpringApplication.class, args);
    }
}
