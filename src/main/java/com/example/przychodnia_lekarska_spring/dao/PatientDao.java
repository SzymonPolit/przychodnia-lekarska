package com.example.przychodnia_lekarska_spring.dao;

import com.example.przychodnia_lekarska_spring.model.Patient;
import org.springframework.data.repository.CrudRepository;

public interface PatientDao extends CrudRepository<Patient, Long> {

}
