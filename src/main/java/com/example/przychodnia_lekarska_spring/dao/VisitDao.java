package com.example.przychodnia_lekarska_spring.dao;

import com.example.przychodnia_lekarska_spring.model.Patient;
import com.example.przychodnia_lekarska_spring.model.Visit;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VisitDao extends CrudRepository<Visit, Long> {

}
