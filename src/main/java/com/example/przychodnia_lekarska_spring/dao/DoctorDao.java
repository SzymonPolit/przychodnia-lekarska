package com.example.przychodnia_lekarska_spring.dao;

import com.example.przychodnia_lekarska_spring.model.Doctor;
import org.springframework.data.repository.CrudRepository;

public interface DoctorDao extends CrudRepository<Doctor, Long> {
}