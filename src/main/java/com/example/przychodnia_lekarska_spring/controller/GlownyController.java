package com.example.przychodnia_lekarska_spring.controller;


import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class GlownyController {

    //STRONA GŁÓWNA
    @GetMapping("/index")
    public String mainpage(){
        return "/przychodnia/index";
    }

}
