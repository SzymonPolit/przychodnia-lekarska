package com.example.przychodnia_lekarska_spring.controller;

import com.example.przychodnia_lekarska_spring.dao.DoctorDao;
import com.example.przychodnia_lekarska_spring.model.Doctor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class DoctorController {

    @Autowired
    DoctorDao doctorDao;

    //1. TWORZENIE NOWEGO LEKARZA
    @GetMapping("/nowydoctor")
    public String newdoctort() {
        return "/przychodnia/newdoctor";
    }


    //2. PRZEKIEROWANIE DO WSZYSTKICH LEKARZY(zapis do bazy danych)
    @PostMapping("/lekarze")
    public String savedoctor(@ModelAttribute Doctor doctor) {
        doctorDao.save(doctor);
        return "redirect:/wszystkiedoktorki";
    }

    //3. WYŚWIETLENIE WSZYSTKICH LEKRARZY
    @GetMapping("/wszystkiedoktorki")
    public String alldoctors(ModelMap modelMap) {
        modelMap.put("kluczyk", doctorDao.findAll());
        return "/przychodnia/alldoctors";
    }

}
