package com.example.przychodnia_lekarska_spring.controller;

import com.example.przychodnia_lekarska_spring.dao.PatientDao;
import com.example.przychodnia_lekarska_spring.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class PatientController {

    @Autowired
    PatientDao patientDao;

    //1. TWORZENIE NOWEGO PACJENTA
    @GetMapping("/nowypacjent")
    public String newpatient() {
        return "/przychodnia/newpatient";
    }

    //2. ZAPIS W BAZIE DANYCH ORAZ PRZEKIEROWANIE
    @PostMapping("/pacjenci")
    public String savepatient(@ModelAttribute Patient patient) {
        patientDao.save(patient);
        return "redirect:/wszyscypacjenci";
    }
    //3. WYŚWIETLENIE WSZYSTKICH PACJENTÓW
    @GetMapping("/wszyscypacjenci")
    public String allpatients(ModelMap modelMap) {
        modelMap.put("kluczyk", patientDao.findAll());
        return "/przychodnia/allpatients";
    }



}
