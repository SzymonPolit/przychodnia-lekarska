package com.example.przychodnia_lekarska_spring.controller;


import com.example.przychodnia_lekarska_spring.dao.DoctorDao;
import com.example.przychodnia_lekarska_spring.dao.PatientDao;
import com.example.przychodnia_lekarska_spring.dao.VisitDao;
import com.example.przychodnia_lekarska_spring.model.Visit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class VisitControllor {

    @Autowired
    VisitDao visitDao;
    @Autowired
    PatientDao patientDao;
    @Autowired
    DoctorDao doctorDao;

    //1. TWORZENIE NOWEJ WIZYTY I PRZEKIEROWANIE
    @GetMapping("/nowawizyta")
    public String newvisit(ModelMap modelMap) {
        modelMap.put("kluczyk1", patientDao.findAll());
        modelMap.put("kluczyk2", doctorDao.findAll());

        return "/przychodnia/newvisit";
    }

    //2. PRZEKIEROWANIE
    @PostMapping("/wizyty")
    public String nowawizyta(@ModelAttribute Visit visit) {
        visitDao.save(visit);
        return "redirect:/wszystkiewizyty";
    }

    //3. WYŚWIETLENIE WSZYSTKICH WIZYT
    @GetMapping("/wszystkiewizyty")
    public String allvisits(ModelMap modelMap) {
        modelMap.put("kluczyk10", visitDao.findAll());
        return "/przychodnia/allvisits";
    }



}
